package org.fundacionjala.app;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.fundacionjala.app.quizz.model.QuizAnswers;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.stream.JsonReader;

public class JsonQuizc {

	public static void writeJsonFile(QuizAnswers quizAnswers) {
        Gson gson = new Gson();
        try (Writer writer = new FileWriter("./myForm.json")) {
            gson.toJson(quizAnswers, writer);
        } catch (JsonIOException | IOException exception) {
            exception.printStackTrace();
        }
    }
	
	public static QuizAnswers readJsonFile() {
        Gson gson = new Gson();
        QuizAnswers quizAnswers = null;
        try (JsonReader reader = new JsonReader(new FileReader("./myForm.json"))) {
        	quizAnswers = gson.fromJson(reader, QuizAnswers.class);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return quizAnswers;
    }
}
