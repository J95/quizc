package org.fundacionjala.app.quizz.model.configuration;

import org.fundacionjala.app.quizz.model.validator.ValidatorType;

public class NumericConfiguration extends QuestionConfiguration {

	//Adding New Validations to the NumericComfiguration
	public NumericConfiguration() {
        super(false, ValidatorType.REQUIRED, ValidatorType.MIN);
    }

}
