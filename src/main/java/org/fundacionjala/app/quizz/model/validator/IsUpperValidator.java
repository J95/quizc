package org.fundacionjala.app.quizz.model.validator;

import java.util.List;

public class IsUpperValidator implements Validator {

	private static final String ERROR_MESSAGE = "The value is not in uppercase";

	@Override
    public void validate(String value, String conditionValue, List<String> errors) {
		for(char letter: value.toCharArray ())
		{
			String letterstr = Character. toString(letter);
			if (!letterstr.equals(letterstr.toUpperCase())) {
	            errors.add(ERROR_MESSAGE);
	        }
		}     
    }
}
