package org.fundacionjala.app.quizz.model.validator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ValidatorIT {
	
	List<String> errors;
	
	@Before
	public void before(){
		errors = new ArrayList<>();
	}

    @Test
    public void testDateValidator() {       
        Validator dateValidator = ValidatorType.DATE.getValidator();
        dateValidator.validate("24/12/2021", null, errors);
        Assert.assertEquals(DateValidator.class, dateValidator.getClass());
        Assert.assertTrue(errors.isEmpty());
    }
    
    @Test
    public void testRequieredValidator() {
        Validator requiredValidator = ValidatorType.REQUIRED.getValidator();
        requiredValidator.validate("hello", null, errors);
        Assert.assertEquals(RequiredValidator.class, requiredValidator.getClass());
        Assert.assertTrue(errors.isEmpty());
    }
    
    @Test
    public void testMinValidator() {
        Validator minValidator = ValidatorType.MIN.getValidator();
        minValidator.validate("7", "3", errors);
        Assert.assertEquals(MinValidator.class, minValidator.getClass());
        Assert.assertTrue(errors.isEmpty());
    }
    
    @Test
    public void testMinLengthValidator() {
        Validator minLengthValidator = ValidatorType.MIN_LENGTH.getValidator();
        minLengthValidator.validate("hello", "3", errors);
        Assert.assertEquals(MinLengthValidator.class, minLengthValidator.getClass());
        Assert.assertTrue(errors.isEmpty());
    }
    
    @Test
    public void testMaxValidator() {	
        Validator maxValidator = ValidatorType.MAX.getValidator();
        maxValidator.validate("3", "7", errors);
        Assert.assertEquals(MaxValidator.class, maxValidator.getClass());
        Assert.assertTrue(errors.isEmpty());
    }
    
    @Test
    public void testMaxLengthValidator() {
        Validator maxLengthValidator = ValidatorType.MAX_LENGTH.getValidator();
        maxLengthValidator.validate("hello", "9", errors);
        Assert.assertEquals(MaxLengthValidator.class, maxLengthValidator.getClass());
        Assert.assertTrue(errors.isEmpty());
    }
    
    @Test
    public void testIsUpperValidator() {
        Validator isUpperValidator = ValidatorType.UPPER.getValidator();
        isUpperValidator.validate("HELLO", null, errors);
        Assert.assertEquals(IsUpperValidator.class, isUpperValidator.getClass());
        Assert.assertTrue(errors.isEmpty());
    }
    
  
    
}
